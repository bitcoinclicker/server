<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Helpers\Settings;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Settings::$TABLE_OBJECT, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_ID_')->nullable()->comment('ID записи');
            $table->String('_CITY_')->nullable()->comment('город');
            $table->String('_ADRS_')->nullable()->comment('адрес (улица дом)');
            $table->String('_OBJ_TYPE_')->nullable()->comment('Тип помещения');

            $table->decimal('_TTL_S_')->nullable()->comment('общая площадь кв.м.');
            $table->decimal('_F1_S_')->nullable()->comment('площадь 1-го этажа кв.м.');
            $table->String('_F1_U_')->nullable()->comment('назначение площадей 1-го этажа');
            $table->decimal('_FC_S_')->nullable()->comment('площадь цоколя кв.м.');
            $table->String('_FC_U_')->nullable()->comment('назначение площадей цоколя');
            $table->decimal('_FO_S_')->nullable()->comment('площадь подвала кв.м.');
            $table->String('_FO_U_')->nullable()->comment('назначение площадей подвала');
            $table->decimal('_FA_S_')->nullable()->comment('площадь антресоли кв.м.');
            $table->String('_FA_U_')->nullable()->comment('назначение площадей антресоли');
            $table->decimal('_F2_S_')->nullable()->comment('площадь 2-го этажа кв.м.');
            $table->String('_F2_U_')->nullable()->comment('назначение площадей 2-го этажа');
            $table->decimal('_F3_S_')->nullable()->comment('площадь этажей больше 2-го кв.м.');
            $table->String('_F3_U_')->nullable()->comment('назначение площадей этажей больше 2-го');

            $table->String('_AREA_')->nullable()->comment('Район расположения в городе (центр, окраина, деловой центр и т. д.)');
            $table->String('_CHARACT_')->nullable()->comment('Характеристика улицы, проспекта, переулка и т.п, где располагается помещение');
            $table->String('_LINE_')->nullable()->comment('Линия расположения по отношению к улице, проспекту, переулку ит.п, где располагается помещение');
            $table->String('_METRO_')->nullable()->comment('Удаленность от метро');
            $table->String('_ROUND_')->nullable()->comment('Окружение');
            $table->String('_FOOT_TRAF_')->nullable()->comment('Пешеходный трафик');
            $table->String('_IS_PRKNG_')->nullable()->comment('Наличие и тип парковки');
            $table->String('_IS_WIN_')->nullable()->comment('Наличие витринных окон');
            $table->String('_IS_SEP_ENT_')->nullable()->comment('Наличие отдельного входа');
            $table->String('_IS_VENT_')->nullable()->comment('Наличие и тп системы центральной вентиляции и кондиционирования');
            $table->String('_DECOR_')->nullable()->comment('Уровень отделки');
            $table->String('_IS_COM_')->nullable()->comment('Наличие коммуникаций');
            $table->String('_F1_H_')->nullable()->comment('Высота потолков 1-го этажа');
            $table->timestamp('_DATE_')->nullable()->comment('Дата оценки');

            $table->bigInteger('PRICE')->nullable()->comment('Ориентировочная цена (надо определить)');
            $table->decimal('lat', 10, 8 )->nullable();
            $table->decimal('lng', 11, 8 )->nullable();
//            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Settings::$TABLE_OBJECT);
    }
}
