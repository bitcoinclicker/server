<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Helpers\Settings;

class CreateAnalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Settings::$TABLE_ANALOG, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_ID_')->nullable()->comment('ID записи');
            $table->String('_TYPE_')->nullable()->comment('Тип помещения(офис, склад...)');
            $table->timestamp('_DATE_')->nullable()->comment('дата');
            $table->String('_ADDRESS_')->nullable()->comment('адрес');

            $table->bigInteger('_PRICE_')->nullable()->comment('цена объекта');
            $table->decimal('_AREA_')->nullable()->comment('полная пощадь');
            $table->String('_METRO_')->nullable()->comment('Список (если имеются) ближайших станций метро с расстоянием до них в формате — станция1-расстояние1;станция2-расстояние2;...');
            $table->decimal('_LAT_', 10, 8 )->nullable()->comment('широта');
            $table->decimal('_LON_', 11, 8)->nullable()->comment('долгота');
            $table->text('_DESC_')->nullable()->comment('полное описание');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Settings::$TABLE_ANALOG);
    }
}
