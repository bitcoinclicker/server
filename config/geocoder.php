<?php
/**
 * User: Alexandr Zheleznyakov delta.agent00@gmail.com
 * Date: 06.12.2016
 * Time: 14:36
 */

use Ivory\HttpAdapter\CurlHttpAdapter;
use Ivory\HttpAdapter\Guzzle6HttpAdapter;
use Geocoder\Provider\Chain;
use Geocoder\Provider\BingMaps;
use Geocoder\Provider\FreeGeoIp;
use Geocoder\Provider\GoogleMaps;
use Geocoder\Provider\MaxMindBinary;

return [
    'providers' => [
        Chain::class => [
            GoogleMaps::class => [
                'ru',
                'ru',
                true,
                env('GOOGLE_MAPS_API_KEY'),
            ],
            FreeGeoIp::class  => [],
        ],
        BingMaps::class => [
            'en-US',
            env('BING_MAPS_API_KEY'),
        ],
        GoogleMaps::class => [
            'en',
            'us',
            true,
            env('GOOGLE_MAPS_API_KEY'),
        ],
    ],
    'adapter'  => Ivory\HttpAdapter\CurlHttpAdapter::class,
];