<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>

        </style>

        <script>

        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            {{--@if (Route::has('api/loadData'))--}}
            {{ Form::open(['url' => 'api/loadData', 'method' => 'post', 'files' => true]) }}
            {{ Form::file('file') }}
            {{ Form::submit('Send') }}
            {{ Form::close() }}
            {{--@endif--}}
        </div>

        <div class="flex-center position-ref full-height">
            {{--@if (Route::has('api/loadData'))--}}
            {{ Form::open(['url' => 'api/loadDataAnalog', 'method' => 'post', 'files' => true]) }}
            {{ Form::file('fileA') }}
            {{ Form::submit('Send') }}
            {{ Form::close() }}
            {{--@endif--}}
        </div>
    </body>
</html>
