<?php
/**
 * User: Alexandr Zheleznyakov delta.agent00@gmail.com
 * Date: 06.12.2016
 * Time: 9:59
 */

namespace App;

use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Http\Request;

class Applicant
{
    protected $EARTH_RADIUS = 6371; //км 3 963 mi
    protected $price_1_s_meter = 0.0;
    protected $analogs;

    protected $request;

    protected $response;

    public function  __construct(Request $request){
        $this -> request = $request;
    }

    public function startAnalysis(){

        $object = Object::find(1); // 1 - lat : 59.915964 lng: 30.314497;
//        $coords = Geocoder::geocode($object -> _ADRS_ . " " . $object -> _CITY_) -> get() -> first() -> getCoordinates();
//        $this -> response = ['address' => $object -> _ADRS_ . ', ' . $object -> _CITY_, 'lat' => $coords -> getLatitude(), 'lng' => $coords -> getLongitude()];
        $object -> lat = 59.915964;
        $object -> lng = 30.314497;

        $applicants = [];
        $sorted = false;

        $analogs = Analog::all();
//      Ищем среди всех аналогов самые близкие
        foreach ( $analogs as $item) {
            $dist = $this -> getDist($object -> lat, $object -> lng, $item -> _LAT_, $item -> _LON_);

            if ( count($applicants) < 3 ){
                $obj = new \stdClass();
                $obj -> analog = $item;
                $obj -> dist = $dist;
                $applicants[] = $obj;
            }else{
                foreach ($applicants as &$tmp){
                    if ( $tmp -> dist > $dist ){
                        $obj = new \stdClass();
                        $obj -> analog = $item;
                        $obj -> dist = $dist;
                        $tmp = $obj;
                        break;
                    }
                }
            }

            if ( count($applicants) == 3 && !$sorted ){
                $applicants = $this -> bubble_sort($applicants);
                $sorted = true;
            }
        }
//-------------------------------------------------------------------------------
//      По каждому ближайшему аналогу парсим описание
        $desc = "";
        foreach( $applicants as $item ){
            $desc = $item -> analog -> _DESC_;
            break;
        }
        $this -> response = ["dist" => $applicants];
    }

    function bubble_sort($arr) {
        $size = count($arr);
        for ($i=0; $i<$size; $i++) {
            for ($j=0; $j<$size-1-$i; $j++) {
                if ($arr[$j+1] -> dist < $arr[$j] -> dist) {
                    $this -> swap($arr, $j, $j+1);
                }
            }
        }
        return $arr;
    }

    function swap(&$arr, $a, $b) {
        $tmp = $arr[$a];
        $arr[$a] = $arr[$b];
        $arr[$b] = $tmp;
    }

    private function getDist($src_lat, $src_lon, $dst_lat, $dst_lon){
        return $this -> EARTH_RADIUS * 2 * asin(sqrt(
            pow(sin(($src_lat - abs($dst_lat)) * pi()/180 / 2), 2) +
            cos($src_lat * pi()/180) *
            cos(abs($dst_lat) * pi()/180) *
            pow(sin(($src_lon - $dst_lon) * pi()/180 / 2), 2)
        ));
    }

    public function response(){
        return response() -> json($this -> response);
    }
}