<?php

namespace App;

use App\Helpers\Settings;
use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
    protected $dates = ['_DATE_', 'created_at', 'updated_at'];

    protected $table;
    protected $error;

    public function __construct( $attributes = array() ){
        $this -> table = Settings::$TABLE_OBJECT;
//        $this -> error = Errors::$ERROR_NO;

        parent::__construct($attributes);
    }
}
