<?php
/**
 * User: Alexandr Zheleznyakov delta.agent00@gmail.com
 * Date: 03.12.2016
 * Time: 16:50
 */

namespace App;

use Illuminate\Support\Facades\Input;


class ObjectListImport extends \Maatwebsite\Excel\Files\ExcelFile{

    protected $delimiter  = ',';
    protected $enclosure  = '"';
    protected $lineEnding = '\r\n';


    public function getFile()
    {
        // Import a user provided file
        $file = Input::file('file');
        $filename = $file ->getRealPath();

        // Return it's location
        return $filename;
    }
}