<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Settings;

class Analog extends Model
{
    protected $dates = ['_DATE_', 'created_at', 'updated_at', 'deleted_at'];

    protected $table;
    protected $error;

    public function __construct( $attributes = array() ){
        $this -> table = Settings::$TABLE_ANALOG;
//        $this -> error = Errors::$ERROR_NO;

        parent::__construct($attributes);
    }
}
