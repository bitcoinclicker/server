<?php
/**
 * User: Alexandr Zheleznyakov delta.agent00@gmail.com
 * Date: 03.12.2016
 * Time: 17:43
 */

namespace App\Helpers;

use App\Analog;
use App\Object;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class LoadData
{
    public function loadData($file){

        Excel::load($file -> getRealPath(), function($reader){
            $reader -> toObject() -> each(function($row) {
                $o = new Object;

                $o -> _ID_ = $this -> checkNumeric($row -> id);
                $o -> _CITY_ = $row -> city;
                $o -> _ADRS_ = $row -> adrs;
                $o -> _OBJ_TYPE_ = $row -> obj_type;

                $o -> _TTL_S_ = $this -> checkNumeric($row -> ttl_s);
                $o -> _F1_S_ = $this -> checkNumeric($row -> f1_s);
                $o -> _F1_U_ = $row -> f1_u;
                $o -> _FC_S_ = $this -> checkNumeric($row -> fc_s);
                $o -> _FC_U_ = $row -> fc_u;
                $o -> _FO_S_ = $this -> checkNumeric($row -> fo_s);
                $o -> _FO_U_ = $row -> fo_u;
                $o -> _FA_S_ = $this -> checkNumeric($row -> fa_s);
                $o -> _FA_U_ = $row -> fa_u;
                $o -> _F2_S_ = $this -> checkNumeric($row -> f2_s);
                $o -> _F2_U_ = $row -> f2_u;
                $o -> _F3_S_ = $this -> checkNumeric($row -> f3_s);
                $o -> _F3_U_ = $row -> f3_u;
                $o -> _AREA_ = $row -> area;
                $o -> _CHARACT_ = $row -> charact;
                $o -> _LINE_ = $row -> line;
                $o -> _METRO_ = $row -> metro;
                $o -> _ROUND_ = $row -> round;
                $o -> _FOOT_TRAF_ = $row -> foot_traf;
                $o -> _IS_PRKNG_ = $row -> is_prkng;
                $o -> _IS_WIN_ = $row -> is_win;
                $o -> _IS_SEP_ENT_ = $row -> is_sep_ent;
                $o -> _IS_VENT_ = $row -> is_vent;
                $o -> _DECOR_ = $row -> decor;
                $o -> _IS_COM_ = $row -> is_com;
                $o -> _F1_H_ = $this -> checkNumeric($row -> f1_h);
                $o -> _DATE_ = $this -> checkDate($row -> date);

                $o -> save();
            });
        });
    }

    public function loadDataAnalog($file){
        Excel::load($file -> getRealPath(), function($reader){
//            $max_result = 17587;
//            $res = $reader -> skip(13001) -> take(1) -> get();
//            $res = $reader -> get();
//            print_r($res);
//            echo "--> " . count($res) . " <--";

            $reader -> toObject() -> each(function($row) {
                $o = new Analog;

                $o -> _ID_ = $this -> checkNumeric($row -> id);
                $o -> _TYPE_ = $row -> type;
                $o -> _DATE_ = $this -> checkDate($row -> date);
                $o -> _ADDRESS_ = $row -> address;
                $o -> _PRICE_ = $this -> checkNumeric($row -> price);

                $o -> _AREA_ = $this -> checkNumeric($row -> area);
                $o -> _METRO_ = $row -> metro;
                $o -> _DESC_ = $row -> desc;
                $o -> _LAT_ = $this -> checkNumeric($row -> lat);
                $o -> _LON_ = $this -> checkNumeric($row -> lon);

                $o -> save();
            });
        });
    }

    private function checkNumeric($number){
        $number = str_replace(',', '.' , $number);
        $number = (float)filter_var( $number, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        if ( !is_numeric($number) ){
            return 0;
        }else{
            return $number;
        }
    }

    private function checkDate($date){
        $tmp = explode('.', $date);

        if (count($tmp) < 3){
            $tmp = explode('/', $date);
        }
        if (count($tmp) < 3){
            $tmp = explode('-', $date);
        }

        if ( count($tmp) == 3 ){
            return Carbon::create( strlen($tmp[2]) == 4 ? $tmp[2] : ($tmp[2] + 2000), $tmp[1], $tmp[0], 0, 0 ,0);
        }else{
            return Carbon::now();
        }
    }
}