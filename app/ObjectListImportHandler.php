<?php
/**
 * User: Alexandr Zheleznyakov delta.agent00@gmail.com
 * Date: 03.12.2016
 * Time: 17:22
 */

namespace App;


class ObjectListImportHandler implements \Maatwebsite\Excel\Files\ImportHandler {

    public function handle(ObjectListImport $import)
    {
        // get the results
        $results = $import->get();
    }
}