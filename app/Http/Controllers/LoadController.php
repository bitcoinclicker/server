<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ObjectListImport;
use App\Helpers\LoadData;

class LoadController extends Controller
{
    public function loadFile( Request $request ){
        if ( $request->hasFile('file') && $request->file('file')->isValid() ){
            $file = $request->file('file');

            $loadObject = new LoadData();
            $loadObject -> loadData($file);
        }

        return response() -> json( $request -> all() );
    }

    public function loadFileAnalog( Request $request ){
        if ( $request->hasFile('fileA') && $request->file('fileA')->isValid() ){
            $file = $request->file('fileA');

            $loadObject = new LoadData();
            $loadObject -> loadDataAnalog($file);
        }

        return response() -> json( $request -> all() );
    }

//    public function loadFile( ObjectListImport $request ){
////        $request->handleImport();
//        $results = $request->get();
//
//        $importData = $results->toArray();
//
//        print_r($importData[0]);
//
////        $request -> parse();
////        print_r($results);
//
//        return response() -> json( $importData[0] );
//    }
}
