<?php

namespace App\Http\Controllers;

use App\Applicant;
use Illuminate\Http\Request;

class AnalysisController extends Controller
{
    public function getResult( Request $request ){
        $applicants = new Applicant($request);

        $applicants -> startAnalysis();

        return $applicants -> response();
    }
}
